# frozen_string_literal: true

# Ensure the bot will run
at_exit { @bot&.run }

require 'discordrb'
require_relative 'lib/Language.rb'
# require_relative 'lib/ReactionRole.rb'
require_relative 'lib/BotExtension.rb'
require_relative 'lib/MessageLinkFilter.rb'

unless ENV['TOKEN']
  puts 'Chat-Tan require a token to be able to run'
  exit
end

@bot = Discordrb::Commands::CommandBot.new(
  token: ENV['TOKEN'],
  prefix: '/',
  advanced_functionality: true,
  previous: '',
  chain_delimiter: '',
  chain_args_delim: '',
  sub_chain_start: '',
  sub_chain_end: ''
)

# Ready event
@bot.ready do |event|
  # @type [Discordrb::Commands::CommandBot]
  bot = event.bot
  bot.game = 'Assign SDK roles in #psdk-access'
  # bot.dnd

  bot.find_channel('bot').each { |channel| bot.add_channel(channel) }
  bot.find_channel('psdk-access').each { |channel| bot.add_channel(channel) }
  # ReactionRoleHandle.new(bot)
  MessageLinkFilter.new(bot)
  # Define the stop command
=begin
  bot.command(:stop, description: 'Stops the bot.') do |initiator|
    if initiator.user.id == 0x1FF02B372800000
      initiator.message.delete rescue nil
      initiator.send_embed { |embed| embed.image = Discordrb::Webhooks::EmbedImage.new(url: 'https://media1.tenor.com/images/dbb9206a3ba0eda2ad2e4f7a4f2f8674/tenor.gif') }
      bot.stop
    else
      lang = Language.get(initiator.user)
      initiator.send_message(lang.dig(:generic, :yourenotmymaster))
    end
  end
=end
end
