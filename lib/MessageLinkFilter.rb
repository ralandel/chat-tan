# Class responsive of filtering message out
class MessageLinkFilter
  ALLOWED_DOMAINS = %w[
    pokemonworkshop.fr
    pokemonworkshop.com
    twitter.com
    t.co
    reliccastle.com
    www.pokecommunity.com
    gitlab.com
    app.clickup.com
    www.rubydoc.info
    ruby-doc.org
    www.deviantart.com
    www.spriters-resource.com
    twitch.tv
    www.twitch.tv
    www.youtube.com
    youtu.be
    cdn.discordapp.com
    www.google.com
    discord.gg
    discord.com
    www.instagram.com
    instagram.com
    www.soundcloud.com
    soundcloud.com
    www.pokepedia.fr
    pokemondb.net
    bulbapedia.bulbagarden.net
    www.figma.com
    figma.com
    www.webtoons.com
    psdk.pokemonworkshop.fr
    download.psdk.pokemonworkshop.fr
    download.psdk.pokemonworkshop.com
    discordapp.com
    media.discordapp.net
    media.tenor.co
    images-ext-2.discordapp.net
    media1.giphy.com
    vm.tiktok.com
    tenor.com
    github.io
    wikipedia.org
    itch.io
    puu.sh
    q2a.pokemonsdk.com
  ]
  NO_KICK_TLDS = %w[fr com net eu info org]
  STAFF = 'staff'
  # Create a new ReactionRoleHandler
  # @param bot [Discordrb::Commands::CommandBot] the bot that will receive all the commands
  def initialize(bot)
    @bot = bot
    setup_event
  end

  private

  # Setup the watch event over the message
  def setup_event
    @bot.message(contains: 'http') do |event|
      links = event.content.split('http').select { |c| c.match?(%r{^s*://}) }
      next if links.none?
      next if event.author.roles.any? { |role| role.name.downcase == STAFF }

      domains = links.map { |c| c.match(%r{://([^/ ]+)})[1] }.compact
      invalid_domains = domains.reject { |domain| ALLOWED_DOMAINS.include?(domain) }
      tlds = invalid_domains.map { |c| c.match(/\.([a-z]+)$/)[1] }.compact
      # TLD check
      invalid_tlds = tlds.reject { |tld| NO_KICK_TLDS.include?(tld) }
      if invalid_tlds.any?
        event.message.delete("Invalid TLD : #{invalid_tlds.join(', ')}")
        begin
          event.server.kick(event.user, "Posted link with invalid tld: #{domains.join(', ')}")
        rescue Discordrb::Errors::NoPermission
          puts "Could not kick #{event.author.username}"
        end
        next
      end
      # Domain check
      if invalid_domains.any?
        event.message.delete("Invalid domain : #{invalid_domains.join(', ')}")
        event << "This link is not allowed. Please read the <#360855474284789772> #{event.author.mention}"
      end
    end
  end
end
