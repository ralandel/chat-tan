# frozen_string_literal: true

require 'yaml'

# Class that helps to find the language for a specific user in order to display the right messages
class Language
  # Create a new language object
  # @param filename [String] name of the file to load in order to get the data
  def initialize(filename)
    @data = YAML.load(File.read(filename, encoding: 'utf-8'))
  end

  # Find a string by path
  # @param path [Array<Symbol>]
  # @return [String]
  def dig(*path)
    result = @data.dig(*path)
    unless result
      return Language.languages.default.dig(*path) if self != Language.languages.default
    end
    return result || "#{path.join('.')} string not found..."
  end

  @languages = Dir['lang/*.yml'].map { |lang| [lang.match(%r{lang/(.*)\.yml})[1], Language.new(lang)] }.to_h
  @languages.default = @languages['en'] || @languages.values.first

  class << self
    attr_reader :languages

    # Return the language for a specific user
    # @param user [Discordrb::Member] user we want the language
    # @return [Language]
    def get(user)
      langage = user.roles.find { |role| role.name.include?('lang ') || role.name.include?('SDK-') }&.name&.split(/ |-/)&.last || 'en'
      return @languages[langage.downcase]
    end
  end
end
