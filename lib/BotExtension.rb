# frozen_string_literal: true

# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Lint/Void
# rubocop:disable Style/ClassAndModuleChildren
# rubocop:disable Style/Documentation

# This file is about adding some utility commands to the bot
0

module Discordrb::Commands
  class CommandBot
    # Execute a task that can fail
    # @param event [Discordrb::Events::Respondable]
    # @param lang [Language] language of the user
    # @param path [Array<Symbol>] path to show the error text
    # @param format_opts [Hash] format parameter in case message needs to be yielded
    # @yield [] Block that can fail
    def failable(event, lang, *path, format_opts: {})
      yield
    rescue StandardError
      error(event, lang, *path, format_opts: format_opts)
    end

    # Send a success message
    # @param event [Discordrb::Events::Respondable]
    # @param lang [Language] language of the user
    # @param path [Array<Symbol>] path to show the error text
    # @param format_opts [Hash] format parameter in case message needs to be yielded
    # @return [nil]
    def success(event, lang, *path, format_opts: {})
      event.send_embed do |embed|
        embed.title = 'Success'
        embed.color = 0x00A040
        embed.description = format(lang.dig(*path), format_opts)
      end
    end

    # Send a failure message
    # @param event [Discordrb::Events::Respondable]
    # @param lang [Language] language of the user
    # @param path [Array<Symbol>] path to show the error text
    # @param format_opts [Hash] format parameter in case message needs to be yielded
    # @return [nil]
    def failure(event, lang, *path, format_opts: {})
      event.send_embed do |embed|
        embed.title = 'Failure'
        embed.color = 0xA0A000
        embed.description = format(lang.dig(*path), format_opts)
      end
    end

    # Send a error message
    # @param event [Discordrb::Events::Respondable]
    # @param lang [Language] language of the user
    # @param path [Array<Symbol>] path to show the error text
    # @param format_opts [Hash] format parameter in case message needs to be yielded
    # @return [nil]
    def error(event, lang, *path, format_opts: {})
      event.send_embed do |embed|
        embed.title = 'Error'
        embed.color = 0xFF00000
        embed.description = format(lang.dig(*path), format_opts)
      end
      return nil
    end
  end
end
